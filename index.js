const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const cors = require('cors');
const fs = require('fs');
const polls = require('./poll.json');
const userList = require('./userData.json');



function updatePoll(filedata) {
  fs.writeFile('poll.json', JSON.stringify(filedata, null, 4), function writeJSON(err) {
    if (err) return console.log(err);
  });
}

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: "http://localhost:3000",
        credentials: true
      }
  });

const PORT = process.env.PORT || 5000;

app.use(express.static(__dirname + '/public'));
app.use(cors())

// const polls = {};

io.on('connection', socket => {
    console.log('new connection established')
  socket.on('createPoll', data => {
    const {poll} = data
    const pollId = generateUniqueId();
    polls[pollId] = {
      question: poll.question,
      options: poll.options.map(option => ({ ...option, votes: 0 })),
      active: true
    };
    updatePoll(polls)
    io.emit('fetchPoll', { pollId, ...polls[pollId] });
  });

  // socket.on('vote', data => {
  //   const { pollId, selectedOption } = data;
  //   if (polls[pollId]) {
  //     const option = polls[pollId].options.find(opt => opt.option === selectedOption);
  //     if (option) {
  //       option.votes++;
  //       io.emit('voteCountUpdated', { pollId, options: polls[pollId].options });
  //     }
  //   }
  // });

  socket.on('fetchuser', ()=>{
    io.emit('userData', userList)
  })

  socket.on('lock-poll', (id)=>{
    polls[id] = {
      ...polls[id],
      active: false
    };
    updatePoll(polls)
    io.emit('fetchPoll', {})
  })

  socket.on('activePoll', ()=>{
    let activeId = Object.keys(polls).filter(data=>{
      return (polls[data]||{}).active===true
    })[0]
    if (activeId) {
      io.emit('fetchPoll', { pollId:activeId, ...polls[activeId] })
    } else {
      io.emit('fetchPoll', {})
    }
    
  })

  socket.on('updatepoll', data => {
    polls[data.id] = {
      ...polls[data.id],
      options: data.options
    }

    updatePoll(polls)
    io.emit('fetchPoll', {pollId:data.id, ...polls[data.id]})
  })

  socket.on('history', () => {
    let history = Object.keys(polls).filter(data=>{
      return (polls[data]||{}).active!==true
    }).map(e=>({...polls[e], id: e}))
    io.emit('fetchHistory', history)
  })
});

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

function generateUniqueId() {
  return Math.random().toString(36).substring(2, 10);
}